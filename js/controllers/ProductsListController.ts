///<reference path="../productive-engine-main.ts"/>

productiveEngineApp.controller("ProductsListController", function($scope, $sce, $stateParams, productsService){

    $scope.categoryName = $stateParams.productCategory;
    $scope.products = productsService.getCategoryProducts($stateParams.productCategory);


});