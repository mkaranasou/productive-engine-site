///<reference path="../productive-engine-main.ts"/>
///<reference path="../definitions.ts"/>
/**
 * Created by m.karanasou on 7/25/2016.
 */


productiveEngineApp.controller("HomeController", function($scope, productsService) {
    $scope.owner = new ProductiveEngine.Person({});
    $scope.company = new ProductiveEngine.Company({name: "Productive Engine", owner: $scope.owner});

    $scope.init = function () {
        if(productsService.getProducts().length == 0){
            productsService.setDemoData();
        }
    };

    $scope.setSelectedProduct = function(prodName){
        console.log(prodName)
        productsService.setSelectedProductByName(prodName);
    };

    $scope.init();
});