///<reference path="../productive-engine-main.ts"/>
/**
 * Created by m.karanasou on 7/25/2016.
 */


productiveEngineApp.controller("ProductDetailsController", function($scope,$sce, $stateParams,productsService){
    $scope.selectedProduct = productsService.getProductByName($stateParams.productName);
    $scope.selectedProductCategory = ProductiveEngine.ProductCategoryEnum[$scope.selectedProduct.category];
    //console.log("$scope.selectedProduct", $scope.selectedProduct);
});