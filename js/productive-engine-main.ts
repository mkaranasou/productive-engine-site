///<reference path="definitions.ts"/>

/**
 * Created by m.karanasou on 7/25/2016.
 */

declare var angular;
const isMobileDevice = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent));
console.log(isMobileDevice, "isMobileDevice")

var productiveEngineApp = angular.module("productiveEngine", [ 'ngResource', 'ui.router', 'ngSanitize'], function (){

});

productiveEngineApp.run( function($rootScope, $state, $location) {});
