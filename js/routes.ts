///<reference path="productive-engine-main.ts"/>
/**
 * Created by m.karanasou on 7/25/2016.
 */
// const isMobileDevice = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent));
// console.log(isMobileDevice, "isMobileDevice")


productiveEngineApp.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/'); // redirect to home if unknown route

    $stateProvider
        .state('productiveEngine', {
            abstract: true,
            url: '/',
            views: {
                'home': {
                    templateUrl: isMobileDevice?'templates/partials/m.home.html':'templates/partials/home.html',
                    controller: 'HomeController'
                }
            }
        })
        .state('productiveEngine.homeMain', {
            url: '',
            views: {
                'homeMain': {
                    templateUrl: isMobileDevice?'templates/partials/m.home-main.html':'templates/partials/home-main.html',
                    controller: 'HomeMainController'
                }
            }
        })
        .state('productiveEngine.about', {
            url: 'about',
            views: {
                'about': {
                    templateUrl: isMobileDevice?'templates/partials/m.about.html':'templates/partials/about.html',
                    controller: 'AboutController'
                }
            }
        })
        .state('productiveEngine.contact', {
            url: 'contact',
            views: {
                'contact': {
                    templateUrl: isMobileDevice?'templates/partials/m.contact.html':'templates/partials/contact.html',
                    controller: 'ContactController'
                }
            }
        })
        .state('productiveEngine.terms', {
            url: 'terms',
            views: {
                'terms': {
                    templateUrl: isMobileDevice?'templates/partials/m.terms.html':'templates/partials/terms.html',
                    controller: 'TermsController'
                }
            }
        })
        .state('productiveEngine.privacy', {
            url: 'privacy',
            views: {
                'privacy': {
                    templateUrl: isMobileDevice?'templates/partials/m.privacy.html':'templates/partials/privacy.html',
                    controller: 'PrivacyController'
                }
            }
        })
        .state('productiveEngine.products', {
            url: 'products',
            views: {
                'products': {
                    templateUrl: isMobileDevice?'templates/partials/m.products.html':'templates/partials/products.html',
                    controller: 'ProductsController'
                },
            }
        })
        .state('productiveEngine.productDetails', {
            url: '/product-details/{productName}',
            views: {
                'productDetails': {
                    templateUrl: isMobileDevice?'templates/partials/m.product-details.html':'templates/partials/product-details.html',
                    controller: 'ProductDetailsController'
                },
            }
        })
        .state('productiveEngine.productsList', {
            url: '/products-list/{productCategory}',
            views: {
                'productsList': {
                    templateUrl: isMobileDevice?'templates/partials/m.products-list.html':'templates/partials/products-list.html',
                    controller: 'ProductsListController'
                },
            }
        })
}]);