/**
 * Created by m.karanasou on 7/25/2016.
 */

module ProductiveEngine{
    export class Helper {

        static randomUuid(len, radix) {
            var CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split("");

            var chars = CHARS, uuid = [], rnd = Math.random;
            radix = radix || chars.length;

            if (len) {
                // Compact form
                for (var i = 0; i < len; i++)
                    uuid[i] = chars[0 | rnd() * radix];
            } else {
                // rfc4122, version 4 form
                var r;

                // rfc4122 requires these characters
                uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
                uuid[14] = '4';

                // Fill in random data.  At i==19 set the high bits of clock sequence as
                // per rfc4122, sec. 4.1.5
                for (var i = 0; i < 36; i++) {
                    if (!uuid[i]) {
                        r = 0 | rnd() * 16;
                        uuid[i] = chars[(i === 19) ? (r & 0x3) | 0x8 : r & 0xf];
                    }
                }
            }

            return uuid.join("");
        }

        static myUniqueId() {
            return `id${Helper.randomUuid(10, null)}`;
        }

        static scopeDigest($scope){
            if(!$scope.$$phase) {
                //$digest or $apply
                $scope.$digest();
            }

        }
    }

    export class Person{
        public name: string;
        public surname: string;
        public email: string;

        constructor(options:any){
            this.name = options.name || "Nikolaos";
            this.surname = options.surnname || "Fragkos";
            this.email = options.email || "productiveengine@gmail.com";
        }
    }

    export class Company{
        public name: string;
        public owner: Person;
        public products;

        constructor(options:any){
            this.name = options.name || "Productive Engine";
            this.owner = options.owner || new Person({});
            this.name = options.products ||[];
        }
    }

    export class Product{
        public id: number;
        public category: ProductCategoryEnum;
        public name:string;
        public description: string;
        public features: string;
        public technologies: string;
        public ides: string;
        public createDate: Date;
        public version: string;
        public url: string;
        public license: string;

        constructor(options:any){
            this.id = options.id || Helper.myUniqueId();
            this.category = options.category;
            this.name = options.name || "";
            this.description = options.description || "";
            this.features = options.features || "";
            this.createDate = options.createDate || new Date();
            this.version = options.version || "0.0.1";
            this.url = options.url || "/#";
            this.license = options.license || "";
        }

    }

    export enum ProductCategoryEnum{
        Web,
        Mobile,
        Desktop
    }
}