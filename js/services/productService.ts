///<reference path="../productive-engine-main.ts"/>
///<reference path="../definitions.ts"/>
/**
 * Created by m.karanasou on 7/25/2016.
 */
productiveEngineApp.factory('productsService', function ($rootScope) {
    var products = [];
    var prodToCategories = {};

    var mitLicense = `
        The MIT License (MIT)
        <p>
            Copyright (c) 2016 Productive Engine
        </p>
        <p>
            Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
            files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, 
            copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to 
            whom the Software is furnished to do so, subject to the following conditions:
        </p>
        <p>
            The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
        </p>
        <p>
            THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
        </p>
    `;

    return {
        setDemoData(){

          prodToCategories[ProductiveEngine.ProductCategoryEnum.Desktop] = [];
          prodToCategories[ProductiveEngine.ProductCategoryEnum.Mobile] = [];
          prodToCategories[ProductiveEngine.ProductCategoryEnum.Web] = [];

            //----------------------------------
            var prodKB = new ProductiveEngine.Product({
                name: "Knowledge Base",
                category: ProductiveEngine.ProductCategoryEnum.Desktop,
                description: "This is a WPF application showcasing the use of the Prism framework.\n" +
                "With this program you can store the the problems that you encounter as a developer with their corresponding solutions.",
                url: "https://github.com/ProductiveEngine/KnowledgeBase",
            });
            prodKB.features = `            
            <ul class="list-group">
                <li class="list-group-item">Basic project structure for WPF projects
                    <ul class="list-group">
                        <li class="list-group-item">MVVM pattern using the Prism framework</li>
                    </ul>
                </li>
                <li  class="list-group-item">Enterprise level Data Access Layer
                    <ul class="list-group">
                         <li class="list-group-item">Repository</li>
                         <li class="list-group-item">Unit of Work</li>
                         <li class="list-group-item">Accessors</li>
                     </ul>
                 </li>
            </ul>`;
            prodKB.technologies = `
                    <ul class="list-group">
                        <li class="list-group-item">
                            WPF
                        </li>
                        <li class="list-group-item">
                            Extended WPF toolkit
                        </li>
                        <li class="list-group-item">
                            Frameworks
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Prism
                                </li>
                                <li class="list-group-item">
                                    Entity
                                </li>
                                <li class="list-group-item">
                                    Unity
                                </li>
                            </ul>                            
                        </li>
                    </ul>                                                              
                     `;
            prodKB.ides = `Microsoft Visual Studio`;
            prodKB.license = mitLicense;
            prodToCategories[ProductiveEngine.ProductCategoryEnum.Desktop].push(prodKB);
            //----------------------------------
            var prodTournament = new ProductiveEngine.Product({
                name: "Tournament",
                category: ProductiveEngine.ProductCategoryEnum.Desktop,
                description: "TBD",
                url: "http://productiveengine.com/wpf/tournament"
            });
            //prodToCategories[ProductiveEngine.ProductCategoryEnum.Desktop].push(prodTournament);

            //----------------------------------
            var prodMYL= new ProductiveEngine.Product({
                name: "Music You Love",
                category: ProductiveEngine.ProductCategoryEnum.Mobile,
                description: `Music you love is an Android&#8482; audio player focused on sorting out which songs you truly love.                    
                              <p>Support us by purchasing the compiled app from Google Play!</p>
                `,
                url: "https://github.com/ProductiveEngine/MYL"
            });
            prodMYL.features =`
            <ul class="list-group">            
                <li class="list-group-item">MVVM pattern using the Android Data Binding Library</li>
                <li class="list-group-item">Audio playback</li>
                <li class="list-group-item">MediaPlayer</li>
                <li class="list-group-item">MediaSessionManager</li>
                <li class="list-group-item">MediaSession</li>
                <li class="list-group-item">MediaController</li>
                <li class="list-group-item">MediaMetadataRetriever</li>
                <li class="list-group-item">Active Android library for accessing a SQLLite database</li>
                <li class="list-group-item">AsyncTask</li>
            </ul>
            `;

            prodMYL.technologies =`Android&#8482;`;
            prodMYL.ides =`Android Studio`;
            prodMYL.license = mitLicense;
            prodToCategories[ProductiveEngine.ProductCategoryEnum.Mobile].push(prodMYL);
            //----------------------------------
            var prodSC= new ProductiveEngine.Product({
                name: "Super Compare",
                category: ProductiveEngine.ProductCategoryEnum.Mobile,
                description: `This is an Android&#8482; application showcasing the basics of android development.
                Super Compare is a Bench marking application for your mobile device.
                It measures how long will it take for your device to execute certain algorithms.
                The sooner it completes the procedure, the better!!!.
                <br>
                <p>Support us by purchasing the compiled app from Google Play!</p>`,
                url: "https://github.com/ProductiveEngine/SuperCompare"
            });
            prodSC.features  =`Android&#8482;
                Basic project structure
                Simple PI calculation for benchmarking purposes`;
            prodSC.technologies =`Android&#8482;`;
            prodSC.ides =`Android Studio`;
            prodSC.license = mitLicense;
            prodToCategories[ProductiveEngine.ProductCategoryEnum.Mobile].push(prodSC);
            //----------------------------------
            var prodMiniEShop = new ProductiveEngine.Product({
                name: "MiniEShop",
                category: ProductiveEngine.ProductCategoryEnum.Web,
                description: `This is a simple web application showcasing the use of the MVC5 and Angular JS 1.x . Tha data access layer has been designed as an enterprise level project.`,
                url: 'https://github.com/ProductiveEngine/MiniEShop'
            });
            prodMiniEShop.features =`
            <ul class="list-group">  
                <li class="list-group-item">Angular JS 1.x
                    <ul class="list-group">
                        <li class="list-group-item">ngResource</li>
                        <li class="list-group-item">ngMessages</li>
                        <li class="list-group-item">ui.router</li>
                    </ul>
                </li>
                <li class="list-group-item">Enterprise level architecture for the data access layer
                    <ul class="list-group">
                        <li class="list-group-item">Repository</li>
                        <li class="list-group-item">Unit of work</li>
                        <li class="list-group-item">Accessors</li>
                    </ul>
                </li>
                <li class="list-group-item">Web Api</li>
            </ul>`;
            prodMiniEShop.technologies =`
                <ul class="list-group"> 
                    <li class="list-group-item">Microsoft MVC5</li>
                    <li class="list-group-item">Microsoft Entity framework 6</li>
                    <li class="list-group-item">Google AngularJS v1.5.7</li>
                    <li class="list-group-item">Bootstrap v3.3.6</li>
                    <li class="list-group-item">jquery-2.2.4</li>
                    <li class="list-group-item">toastr</li>
                </ul>
            `;
            prodMiniEShop.ides =`Microsoft Visual Studio`;
            prodMiniEShop.license = mitLicense;
            prodToCategories[ProductiveEngine.ProductCategoryEnum.Web].push(prodMiniEShop);
            //----------------------------------
            var prodMiniEShop2 = new ProductiveEngine.Product({
                name: "MiniEShop2",
                category: ProductiveEngine.ProductCategoryEnum.Web,
                description: `This is a simple web application showcasing the use of Angular JS 2.`,
                url: "https://github.com/ProductiveEngine/MiniEShop2"
            });
            prodMiniEShop2.features = `
                <ul class="list-group"> 
                    <li class="list-group-item">Angular JS 2
                        <ul class="list-group">
                            <li class="list-group-item">Basic project structure</li>
                            <li class="list-group-item">Master/Detail</li>
                            <li class="list-group-item">Services</li>
                            <li class="list-group-item">Routing</li>
                            <li class="list-group-item">Navigation</li>
                        </ul>
                    </li>
                </ul>`;
            prodMiniEShop2.technologies = `
            <ul class="list-group"> 
                <li class="list-group-item">Google AngularJS v2</li>
                <li class="list-group-item">Bootstrap v3.3.7</li>
            </ul>`;
            prodMiniEShop2.ides = `JetBrains WebStorm`;
            prodMiniEShop2.license = mitLicense;
            prodToCategories[ProductiveEngine.ProductCategoryEnum.Web].push(prodMiniEShop2);
            //----------------------------------

            products.push(prodKB);
            //products.push(prodTournament);
            products.push(prodMYL);
            products.push(prodSC);
            products.push(prodMiniEShop);
            products.push(prodMiniEShop2);

        },
        setProducts(products){
            this.products = products;
        },
        getProducts(){
            return products;
        },
        getProductsForCategory(category:ProductiveEngine.ProductCategoryEnum){
            /*var catProducts = [];
            for(let i=0; i < products.length; i++){

                if(products[i].category == category){
                    catProducts.push(products[i]);
                }
            }
            return catProducts;*/
            return prodToCategories[category];
        },
        getProductByName(productName){

            var selectedProduct;

            for(let i=0; i < products.length; i++){
                if(productName == products[i].name){
                    selectedProduct = products[i];
                    break;
                }
            }

            return selectedProduct;
        },
        getCategoryProducts(productCategory){

            var products;

            if(productCategory.toUpperCase() == "DESKTOP"){
                products = prodToCategories[ProductiveEngine.ProductCategoryEnum.Desktop];
            }
            else if (productCategory.toUpperCase() == "MOBILE"){
                products = prodToCategories[ProductiveEngine.ProductCategoryEnum.Mobile];
            }
            else if (productCategory.toUpperCase() == "WEB"){
                products = prodToCategories[ProductiveEngine.ProductCategoryEnum.Web];
            }

            //$rootScope.$broadcast("product-change");

            return products;
        }
    }
});